#ifndef _SYSTEMCLASS_H_
#define _SYSTEMCLASS_H_

// Pre-processing directives
#define WIN32_LEAN_AND_MEAN

// Includes
#include <windows.h>
#include "Input.h"
#include "Graphics.h"
#include "Sound.h"
#include "Fps.h"
#include "Cpu.h"
#include "Timer.h"

class SystemClass
{
public:
	SystemClass();
	SystemClass(const SystemClass&);
	~SystemClass();

	bool Initialize();
	void Shutdown();
	void Run();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

private:
	bool Frame();
	void InitializeWindows(int&, int&);
	void ShutdownWindows();

private:
	LPCWSTR _applicationName;
	HINSTANCE _hinstance;
	HWND _hwnd;

	Input* _input;
	Graphics* _graphics;
	Sound* _sound;
	Camera* _camera;
	Fps* _fps;
	Cpu* _cpu;
	Timer* _timer;
};

// Function prototypes
static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// Globals
static SystemClass* ApplicationHandle = 0;

#endif