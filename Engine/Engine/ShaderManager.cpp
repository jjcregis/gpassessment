#include "ShaderManager.h"

ShaderManager::ShaderManager()
{
	_lightShader = 0;
	_textureShader = 0;
	_multiTextureShader = 0;
	_bumpMapShader = 0;
	_particleShader = 0;
}

ShaderManager::ShaderManager(const ShaderManager& other)
{
}

ShaderManager::~ShaderManager()
{
}

bool ShaderManager::Initialize(ID3D11Device* device, HWND hwnd, bool useDX11)
{
	bool result;

	// Create and initialize the light shader.
	_lightShader = new LightShader();
	result = _lightShader->Initialize(device, hwnd, useDX11);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the light shader.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the texture shader.
	_textureShader = new TextureShader();
	result = _textureShader->Initialize(device, hwnd, useDX11);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the texture shader.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the multi-texture shader.
	_multiTextureShader = new MultiTextureShader();
	result = _multiTextureShader->Initialize(device, hwnd, useDX11);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the multi-texture shader.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the bump map shader.
	_bumpMapShader = new BumpMapShader();
	result = _bumpMapShader->Initialize(device, hwnd, useDX11);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the bump map shader.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the particle shader.
	_particleShader = new ParticleShader();
	result = _particleShader->Initialize(device, hwnd, useDX11);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the particle shader.", L"Error", MB_OK);
		return false;
	}

	return true;
}

void ShaderManager::Shutdown()
{
	// Release the light shader.
	if(_lightShader)
	{
		_lightShader->Shutdown();
		delete _lightShader;
		_lightShader = 0;
	}

	// Release the texture shader.
	if(_textureShader)
	{
		_textureShader->Shutdown();
		delete _textureShader;
		_textureShader = 0;
	}

	// Release the multi-texture shader.
	if(_multiTextureShader)
	{
		_multiTextureShader->Shutdown();
		delete _multiTextureShader;
		_multiTextureShader = 0;
	}
	
	// Release the bump map shader.
	if(_bumpMapShader)
	{
		_bumpMapShader->Shutdown();
		delete _bumpMapShader;
		_bumpMapShader = 0;
	}

	// Release the particle shader.
	if(_particleShader)
	{
		_particleShader->Shutdown();
		delete _particleShader;
		_particleShader = 0;
	}

	return;
}

bool ShaderManager::RenderLightShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
			      D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture, D3DXVECTOR3 lightDirection, D3DXVECTOR4 ambientColor,
			      D3DXVECTOR4 diffuseColor)
{
	// Render the model using the light shader.
	return _lightShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, texture, lightDirection, ambientColor, diffuseColor);
}

bool ShaderManager::RenderTextureShader(ID3D11DeviceContext* device, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, 
											 ID3D11ShaderResourceView* texture)
{
	// Render the model using the texture shader.
	return _textureShader->Render(device, indexCount, worldMatrix, viewMatrix, projectionMatrix, texture);
}

bool ShaderManager::RenderMultiTextureShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, 
									 D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView** textureArray)
{
	// Render the model using the multi-texture shader
	return _multiTextureShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, textureArray);
}

bool ShaderManager::RenderBumpMapShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, 
											 ID3D11ShaderResourceView** textureArray, D3DXVECTOR3 lightDirection, 
											 D3DXVECTOR4 diffuse, D3DXVECTOR4 ambient)
{
	// Render the model using the bump map shader.
	return _bumpMapShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, textureArray, lightDirection, diffuse, ambient);
}

bool ShaderManager::RenderParticleShader(ID3D11DeviceContext* deviceContext, int indexCount, D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, 
											D3DXMATRIX projectionMatrix, ID3D11ShaderResourceView* texture)
{
	// Render the model using the particle shader.
	return _particleShader->Render(deviceContext, indexCount, worldMatrix, viewMatrix, projectionMatrix, texture);
}