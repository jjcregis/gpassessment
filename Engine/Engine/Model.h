#ifndef _MODEL_H_
#define _MODEL_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <fstream>
#include "TextureArray.h"
using namespace std;

class Model
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
		D3DXVECTOR3 normal;
		D3DXVECTOR3 tangent;
		D3DXVECTOR3 binormal;
	};

	struct ModelType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
		float tx, ty, tz;
		float bx, by, bz;
	};

		struct TempVertexType
	{
		float x, y, z;
		float tu, tv;
		float nx, ny, nz;
	};

	struct VectorType
	{
		float x, y, z;
	};

public:
	Model();
	Model(const Model&);
	~Model();

	bool Initialize(ID3D11Device*, char*, WCHAR*, WCHAR* secondaryFilename = NULL, D3DXVECTOR3 position = D3DXVECTOR3(0, 0, 0), float scale = 1);
	void Shutdown();
	void Render(ID3D11DeviceContext*);
	void CalculateModelVectors();

	int GetIndexCount();
	ID3D11ShaderResourceView** GetTextureArray();
	ID3D11ShaderResourceView* GetPrimaryTexture();
	ID3D11ShaderResourceView* GetSecondaryTexture();

private:
	bool InitializeBuffers(ID3D11Device*, D3DXVECTOR3, float);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);
	
	bool LoadTextures(ID3D11Device*, WCHAR*, WCHAR*);
	void ReleaseTextures();

	bool LoadModel(char*);
	void ReleaseModel();
	
	void CalculateTangentBinormal(TempVertexType, TempVertexType, TempVertexType, VectorType&, VectorType&);
	void CalculateNormal(VectorType, VectorType, VectorType&);

private:
	ID3D11Buffer *_vertexBuffer, *_indexBuffer;
	int _vertexCount, _indexCount;
	ModelType* _model;
	TextureArray* _textureArray;
};

#endif