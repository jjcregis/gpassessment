#ifndef _CAMERA_H_
#define _CAMERA_H_

// Includes
#include <d3dx10math.h>
#include "Input.h"

class Camera
{
public:
	Camera();
	Camera(const Camera&);
	~Camera();

	bool Initialize(Input*);
	void Shutdown();
	void SetPosition(float, float, float);
	void SetRotation(float, float, float);
	void Translate(float, float, float);
	void Rotate(float, float, float);

	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();

	void Render();
	bool Frame(float);
	void GetViewMatrix(D3DXMATRIX&);

private:
	Input* _input;
	D3DXMATRIX _viewMatrix;
	D3DXVECTOR3 _position, _rotation;
};

#endif