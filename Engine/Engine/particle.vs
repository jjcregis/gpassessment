// Globals
cbuffer MatrixBuffer
{
	matrix worldMatrix;
	matrix viewMatrix;
	matrix projectionMatrix;
};

// Types
struct VertexInputType
{
    float4 position : POSITION;
    float2 tex : TEXCOORD0;
	float4 color : COLOR;
};

struct PixelInputType
{
    float4 position : SV_POSITION;
    float2 tex : TEXCOORD0;
	float4 color : COLOR;
};

// Vertex Shader
PixelInputType ParticleVertexShader(VertexInputType input)
{
    PixelInputType output;
    float colorChangeRate = 15;

	input.color.r += sin(input.position.x * colorChangeRate);
	input.color.g += sin(input.position.y * colorChangeRate);
	input.color.b += sin(input.position.z * colorChangeRate);

	input.position.x += sin(input.color.g/500) * 10;
	input.position.y += sin(input.color.b/500) * 10;
	input.position.z += sin(input.color.r/500) * 10;

	// Change the position vector to be 4 units for proper matrix calculations.
    input.position.w = 1.0f;

	// Fade the particle if its alpha is less than 1
	input.color *= input.color.a;

	// Calculate the position of the vertex against the world, view, and projection matrices.
    output.position = mul(input.position, worldMatrix);
    output.position = mul(output.position, viewMatrix);
    output.position = mul(output.position, projectionMatrix);

	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;
    
	// Store the particle color for the pixel shader. 
    output.color = input.color;

    return output;
}