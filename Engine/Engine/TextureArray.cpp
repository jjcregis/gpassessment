#include "TextureArray.h"

TextureArray::TextureArray()
{
	_textures[0] = 0;
	_textures[1] = 0;
}

TextureArray::TextureArray(const TextureArray& other)
{
}

TextureArray::~TextureArray()
{
}

bool TextureArray::Initialize(ID3D11Device* device, WCHAR* primaryFilename, WCHAR* secondaryFilename)
{
	HRESULT result;

	// Load the first texture in.
	result = D3DX11CreateShaderResourceViewFromFile(device, primaryFilename, NULL, NULL, &_textures[0], NULL);
	if(FAILED(result))
		return false;

	// Load the second texture in if one is provided
	if(secondaryFilename != NULL)
	{
		result = D3DX11CreateShaderResourceViewFromFile(device, secondaryFilename, NULL, NULL, &_textures[1], NULL);
		if(FAILED(result))
			return false;
	}

	return true;
}

void TextureArray::Shutdown()
{
	// Release the texture resources.
	if(_textures[0])
	{
		_textures[0]->Release();
		_textures[0] = 0;
	}

	if(_textures[1])
	{
		_textures[1]->Release();
		_textures[1] = 0;
	}

	return;
}

ID3D11ShaderResourceView** TextureArray::GetTextureArray()
{
	return _textures;
}