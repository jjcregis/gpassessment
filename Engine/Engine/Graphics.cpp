#include "Graphics.h"

Graphics::Graphics()
{
	_d3d = 0;
	_light = 0;
	_particleSystem = 0;

	_fpsText = 0;
	_shaderManager = 0;

	_skybox = 0;
	_tinyModel = 0;
	_multiTexturedModel = 0;
	_bumpMappedModel = 0;
	_litModel = 0;
}

Graphics::Graphics(const Graphics& other)
{
}

Graphics::~Graphics()
{
}

bool Graphics::Initialize(Camera* camera, int screenWidth, int screenHeight, HWND hwnd)
{
	bool result;
	bool usingDX11;
	D3DXMATRIX baseViewMatrix;
	_camera = camera;

	// Create and initialize Direct3D.
	_d3d = new D3DCore();
	result = _d3d->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR, usingDX11);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize Direct3D.", L"Error", MB_OK);
		return false;
	}

	// Initialize a base view matrix with the camera for 2D user interface rendering.
	_camera->SetPosition(0.0f, 0.0f, -1.0f);
	_camera->Render();
	_camera->GetViewMatrix(baseViewMatrix);

	// Set the initial position of the camera.
	_camera->SetPosition(0, 0, -9.0f);

	// Create and initialize the light object.
	_light = new Light();
	_light->SetAmbientColor(.3f, .3f, .3f, 1);
	_light->SetDiffuseColor(.8f, .8f, .8f, 1);
	_light->SetDirection(0, 0, 1);

	// Create and initialize the shader manager and all shaders in it.
	_shaderManager = new ShaderManager();
	result = _shaderManager->Initialize(_d3d->GetDevice(), hwnd, usingDX11);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the shader manager object.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the skybox
	_skybox = new Model();
	result = _skybox->Initialize(_d3d->GetDevice(),  "../Engine/data/sphere.txt", L"../Engine/data/stars.dds", NULL, D3DXVECTOR3(0,0,0), -50);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the skybox.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the tiny model
	_tinyModel = new Model();
	result = _tinyModel->Initialize(_d3d->GetDevice(),  "../Engine/data/cube.txt", L"../Engine/data/bett.dds", NULL, D3DXVECTOR3(3,0,0), .1f);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the tiny model.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the lit model
	_litModel = new Model();
	result = _litModel->Initialize(_d3d->GetDevice(),  "../Engine/data/sphere.txt", L"../Engine/data/speaker.dds");
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the lit model.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the multi-textured model
	_multiTexturedModel = new Model();
	result = _multiTexturedModel->Initialize(_d3d->GetDevice(),  "../Engine/data/cube.txt", L"../Engine/data/paper.dds", L"../Engine/data/scribbles.dds", D3DXVECTOR3(3, 0, 0), 1);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the multi-textured model.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the bump mapped model, then calculate the model vectors for rendering
	_bumpMappedModel = new Model();
	result = _bumpMappedModel->Initialize(_d3d->GetDevice(),  "../Engine/data/cube.txt", L"../Engine/data/brick.dds", L"../Engine/data/bricknormal.dds", D3DXVECTOR3(-3, 0, 0), 1);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the bump mapped model.", L"Error", MB_OK);
		return false;
	}
	_bumpMappedModel->CalculateModelVectors();

	// Create and initialize the particle system.
	_particleSystem = new ParticleSystem();
	result = _particleSystem->Initialize(_d3d->GetDevice(), L"../Engine/data/note.dds", D3DXVECTOR3(0, 0, 0));
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the particle system.", L"Error", MB_OK);
		return false;
	}

	// Create and initialize the FPS text object.
	_fpsText = new Text();
	result = _fpsText->Initialize(_d3d->GetDevice(), _d3d->GetDeviceContext(), hwnd, screenWidth, screenHeight, baseViewMatrix, usingDX11);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize the text object.", L"Error", MB_OK);
		return false;
	}

	return true;
}


void Graphics::Shutdown()
{
	// Release the light object.
	if(_light)
	{
		delete _light;
		_light = 0;
	}

	// Release the particle system object.
	if(_particleSystem)
	{
		_particleSystem->Shutdown();
		delete _particleSystem;
		_particleSystem = 0;
	}

	// Release the skybox
	if(_skybox)
	{
		_skybox->Shutdown();
		delete _skybox;
		_skybox = 0;
	}

	// Release the tiny model
	if(_tinyModel)
	{
		_tinyModel->Shutdown();
		delete _tinyModel;
		_tinyModel = 0;
	}

	// Release the lit model
	if(_litModel)
	{
		_litModel->Shutdown();
		delete _litModel;
		_litModel = 0;
	}

	// Release the multi-textured model
	if(_multiTexturedModel)
	{
		_multiTexturedModel->Shutdown();
		delete _multiTexturedModel;
		_multiTexturedModel = 0;
	}

	// Release the bump mapped model
	if(_bumpMappedModel)
	{
		_bumpMappedModel->Shutdown();
		delete _bumpMappedModel;
		_bumpMappedModel = 0;
	}

	// Release the fps text object.
	if(_fpsText)
	{
		_fpsText->Shutdown();
		delete _fpsText;
		_fpsText = 0;
	}

	// Release the D3D object.
	if(_d3d)
	{
		_d3d->Shutdown();
		delete _d3d;
		_d3d = 0;
	}

	return;
}


bool Graphics::Frame(int fps, float frameTime, float totalTime)
{
	bool result;
	
	// Update the frames per second for the FPS display text.
	result = _fpsText->SetFps(fps, _d3d->GetDeviceContext());
	if(!result)
		return false;

	// Update the light direction
	_light->Frame(totalTime);

	// Run the frame processing for the particle system.
	_particleSystem->Frame(frameTime, fps, _d3d->GetDeviceContext());

	// Render the graphics scene.
	result = Render();
	if(!result)
		return false;

	return true;
}


bool Graphics::Render()
{
	D3DXMATRIX worldMatrix, viewMatrix, projectionMatrix, orthoMatrix;
	bool result;

	// Clear the buffers to begin the scene.
	_d3d->BeginScene(0, 0, 0, 1);

	// Generate the view matrix based on the camera's position.
	_camera->Render();

	// Get the world, view, projection and orthographic matrices from the camera and d3d objects.
	_camera->GetViewMatrix(viewMatrix);
	_d3d->GetWorldMatrix(worldMatrix);
	_d3d->GetProjectionMatrix(projectionMatrix);
	_d3d->GetOrthoMatrix(orthoMatrix);

	// Render the skybox
	_skybox->Render(_d3d->GetDeviceContext());
	result = _shaderManager->RenderTextureShader(_d3d->GetDeviceContext(), _skybox->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, _skybox->GetPrimaryTexture());
	if(!result)
		return false;

	// Render the tiny model
	_tinyModel->Render(_d3d->GetDeviceContext());
	result = _shaderManager->RenderTextureShader(_d3d->GetDeviceContext(), _tinyModel->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, _tinyModel->GetPrimaryTexture());
	if(!result)
		return false;

	// Render the lit model
	_litModel->Render(_d3d->GetDeviceContext());
	result = _shaderManager->RenderLightShader(_d3d->GetDeviceContext(), _litModel->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, 
				       _litModel->GetPrimaryTexture(), _light->GetDirection(), _light->GetAmbientColor(), _light->GetDiffuseColor());
	if(!result)
		return false;

	// Render the multi-textured model
	_multiTexturedModel->Render(_d3d->GetDeviceContext());
	result = _shaderManager->RenderMultiTextureShader(_d3d->GetDeviceContext(), _multiTexturedModel->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix,
				     _multiTexturedModel->GetTextureArray());

	// Render the bump mapped model
	_bumpMappedModel->Render(_d3d->GetDeviceContext());
	result = _shaderManager->RenderBumpMapShader(_d3d->GetDeviceContext(), _bumpMappedModel->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, 
		_bumpMappedModel->GetTextureArray(), _light->GetDirection(), _light->GetDiffuseColor(), _light->GetAmbientColor());
	if(!result)
		return false;

	// Turn off alpha blending for HUD and particle rendering
	_d3d->TurnOnAlphaBlending();

	// Turn off the Z buffer to begin all 2D rendering.
	_d3d->TurnZBufferOff();

	// Render the FPS string.
	result = _fpsText->Render(_d3d->GetDeviceContext(), worldMatrix, orthoMatrix);
	if(!result)
		return false;

	_d3d->TurnZBufferOn();

	// Render the particle system
	_particleSystem->Render(_d3d->GetDeviceContext());
	result = _shaderManager->RenderParticleShader(_d3d->GetDeviceContext(), _particleSystem->GetIndexCount(), worldMatrix, viewMatrix, projectionMatrix, 
					  _particleSystem->GetTexture());
	if(!result)
		return false;

	_d3d->TurnOffAlphaBlending();

	// Present the rendered scene to the screen.
	_d3d->EndScene();

	return true;
}