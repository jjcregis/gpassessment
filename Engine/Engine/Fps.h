#ifndef _FPS_H_
#define _FPS_H_

// Linking
#pragma comment(lib, "winmm.lib")

// Includes
#include <windows.h>
#include <mmsystem.h>

class Fps
{
public:
	Fps();
	Fps(const Fps&);
	~Fps();

	void Initialize();
	void Frame();
	int GetFps();

private:
	int _fps, _count;
	unsigned long _startTime;
};

#endif