#ifndef _PARTICLESYSTEM_H_
#define _PARTICLESYSTEM_H_

// Includes
#include <d3d11.h>
#include <d3dx10math.h>
#include <cstdlib>
#include "Texture.h"

class ParticleSystem
{
private:
	struct ParticleType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR3 velocity;
		D3DXVECTOR4 color;
		float lifeTime;
		bool active;
	};

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
		D3DXVECTOR4 color;
	};

public:
	ParticleSystem();
	ParticleSystem(const ParticleSystem&);
	~ParticleSystem();

	bool Initialize(ID3D11Device*, WCHAR*, D3DXVECTOR3);
	void Shutdown();
	bool Frame(float, int, ID3D11DeviceContext*);
	void Render(ID3D11DeviceContext*);

	ID3D11ShaderResourceView* GetTexture();
	int GetIndexCount();

private:
	bool LoadTexture(ID3D11Device*, WCHAR*);
	void ReleaseTexture();

	bool InitializeParticleSystem();
	void ShutdownParticleSystem();

	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();

	void EmitParticles(float, int);
	void UpdateParticles(float);
	void KillParticles();

	bool UpdateBuffers(ID3D11DeviceContext*);
	void RenderBuffers(ID3D11DeviceContext*);

private:
	D3DXVECTOR3 _systemPosition;
	D3DXVECTOR3 _particleVelocityVariation, _particleDeviation;
	float _particleVelocity;
	float _particleSize, _particlesPerSecond;
	int _maxParticles;
	int _maxParticlesAtLowFps;

	int _currentParticleCount;
	float _accumulatedTime;
	int _maxLifeTime;
	int _minFps;

	Texture* _texture;
	ParticleType* _particleList;
	int _vertexCount, _indexCount;
	VertexType* _vertices;
	ID3D11Buffer *_vertexBuffer, *_indexBuffer;
};

#endif