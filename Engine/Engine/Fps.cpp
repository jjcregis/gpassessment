#include "Fps.h"

Fps::Fps()
{
}

Fps::Fps(const Fps& other)
{
}

Fps::~Fps()
{
}

void Fps::Initialize()
{
	_fps = 0;
	_count = 0;
	_startTime = timeGetTime();
	return;
}

void Fps::Frame()
{
	_count++;

	if(timeGetTime() >= (_startTime + 1000))
	{
		_fps = _count;
		_count = 0;
		
		_startTime = timeGetTime();
	}
}

int Fps::GetFps()
{
	return _fps;
}