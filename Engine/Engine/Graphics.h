#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

// Includes
#include "D3DCore.h"
#include "Camera.h"
#include "Model.h"
#include "Light.h"
#include "Text.h"
#include "ParticleSystem.h"
#include "ShaderManager.h"
using namespace std;

// Globals
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = false;
const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;

class Graphics
{
public:
	Graphics();
	Graphics(const Graphics&);
	~Graphics();

	bool Initialize(Camera*, int, int, HWND);
	void Shutdown();
	bool Frame(int, float, float);
	bool Render();

private:
	D3DCore* _d3d;
	Camera* _camera;
	Light* _light;
	ParticleSystem* _particleSystem;
	ShaderManager* _shaderManager;

	Model* _skybox;
	Model* _litModel;
	Model* _multiTexturedModel;
	Model* _bumpMappedModel;
	Model* _tinyModel;

	Text* _fpsText;	
};

#endif