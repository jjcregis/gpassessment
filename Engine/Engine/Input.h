#ifndef _INPUT_H_
#define _INPUT_H_

// Pre-processing directives
#define DIRECTINPUT_VERSION 0x0800

// Linking
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

// Includes
#include <dinput.h>

//Forward declarations
class SystemClass;
class Camera;

class Input
{
public:
	Input();
	Input(const Input&);
	~Input();

	bool Initialize(HINSTANCE, HWND, int, int);
	void Shutdown();
	bool Frame(SystemClass*);
	bool Frame(Camera*, float);

	bool IsKeyPressed(unsigned char);
	void GetMouseLocation(int&, int&);

private:
	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();

private:
	IDirectInput8* _directInput;
	IDirectInputDevice8* _keyboard;
	IDirectInputDevice8* _mouse;

	unsigned char _keyboardState[256];
	DIMOUSESTATE _mouseState;

	int _screenWidth, _screenHeight;
	int _mouseX, _mouseY;
};

#endif