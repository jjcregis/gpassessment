#include "ParticleSystem.h"

ParticleSystem::ParticleSystem()
{
	_texture = 0;
	_particleList = 0;
	_vertices = 0;
	_vertexBuffer = 0;
	_indexBuffer = 0;
}

ParticleSystem::ParticleSystem(const ParticleSystem& other)
{
}

ParticleSystem::~ParticleSystem()
{
}

bool ParticleSystem::Initialize(ID3D11Device* device, WCHAR* textureFilename, D3DXVECTOR3 systemPosition)
{
	bool result;
	_systemPosition = systemPosition;

	// Load the texture that is used for the particles.
	result = LoadTexture(device, textureFilename);
	if(!result)
		return false;

	// Initialize the particle system.
	result = InitializeParticleSystem();
	if(!result)
		return false;

	// Create the buffers that will be used to render the particles with.
	result = InitializeBuffers(device);
	if(!result)
		return false;

	return true;
}

void ParticleSystem::Shutdown()
{
	// Release the buffers.
	ShutdownBuffers();

	// Release the particle system.
	ShutdownParticleSystem();

	// Release the texture used for the particles.
	ReleaseTexture();

	return;
}

bool ParticleSystem::Frame(float frameTime, int frameRate, ID3D11DeviceContext* deviceContext)
{
	bool result;

	// Release old particles.
	KillParticles();

	// Emit new particles.
	EmitParticles(frameTime, frameRate);
	
	// Update the position of the particles.
	UpdateParticles(frameTime);

	// Update the dynamic vertex buffer with the new position of each particle.
	result = UpdateBuffers(deviceContext);
	if(!result)
		return false;

	return true;
}

void ParticleSystem::Render(ID3D11DeviceContext* deviceContext)
{
	// Put the vertex and index buffers on the graphics pipeline to prepare them for drawing.
	RenderBuffers(deviceContext);

	return;
}

ID3D11ShaderResourceView* ParticleSystem::GetTexture()
{
	return _texture->GetTexture();
}

int ParticleSystem::GetIndexCount()
{
	return _indexCount;
}

bool ParticleSystem::LoadTexture(ID3D11Device* device, WCHAR* filename)
{
	bool result;

	// Create and initialize the texture object.
	_texture = new Texture;
	result = _texture->Initialize(device, filename);
	
	return result;
}

void ParticleSystem::ReleaseTexture()
{
	// Release the texture object.
	if(_texture)
	{
		_texture->Shutdown();
		delete _texture;
		_texture = 0;
	}

	return;
}

bool ParticleSystem::InitializeParticleSystem()
{
	int i;
	
	// Set the time since emission to destroy particles, in milliseconds
	_maxLifeTime = 3000;

	// Set the FPS threshold that will reduce the emission rate
	_minFps = 80;

	// Set the random deviation of where the particles can be located when emitted.
	_particleDeviation = D3DXVECTOR3(.5f, .5f, .5f);

	// Set the speed and speed variation of particles.
	_particleVelocity = .35f;
	_particleVelocityVariation = D3DXVECTOR3(0.05f, 0.05f, 0.75f);

	// Set the physical size of the particles.
	_particleSize = .1f;

	// Set the number of particles to emit per second.
	_particlesPerSecond = 50.0f;

	// Set the maximum number of particles allowed in the particle system.
	_maxParticles = 100;
	_maxParticlesAtLowFps = (int)(_maxParticles * .2);

	// Create the particle list.
	_particleList = new ParticleType[_maxParticles];
	if(!_particleList)
		return false;

	// Initialize the particle list.
	for(i=0; i<_maxParticles; i++)
		_particleList[i].active = false;

	// Initialize the current particle count to zero since none are emitted yet.
	_currentParticleCount = 0;

	// Clear the initial accumulated time for the particle per second emission rate.
	_accumulatedTime = 0.0f;

	return true;
}

void ParticleSystem::ShutdownParticleSystem()
{
	// Release the particle list.
	if(_particleList)
	{
		delete [] _particleList;
		_particleList = 0;
	}

	return;
}

bool ParticleSystem::InitializeBuffers(ID3D11Device* device)
{
	unsigned long* indices;
	int i;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
    D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;

	// Set the maximum number of vertices in the vertex array.
	_vertexCount = _maxParticles * 6;

	// Set the maximum number of indices in the index array.
	_indexCount = _vertexCount;

	// Create the vertex array for the particles that will be rendered.
	_vertices = new VertexType[_vertexCount];
	if(!_vertices)
		return false;

	// Create the index array.
	indices = new unsigned long[_indexCount];
	if(!indices)
		return false;

	// Initialize vertex array to zeros at first.
	memset(_vertices, 0, (sizeof(VertexType) * _vertexCount));

	// Initialize the index array.
	for(i=0; i<_indexCount; i++)
		indices[i] = i;

	// Set up the description of the dynamic vertex buffer.
    vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
    vertexBufferDesc.ByteWidth = sizeof(VertexType) * _vertexCount;
    vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
    vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
    vertexData.pSysMem = _vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Now finally create the vertex buffer.
    result = device->CreateBuffer(&vertexBufferDesc, &vertexData, &_vertexBuffer);
	if(FAILED(result))
		return false;

	// Set up the description of the static index buffer.
    indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
    indexBufferDesc.ByteWidth = sizeof(unsigned long) * _indexCount;
    indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    indexBufferDesc.CPUAccessFlags = 0;
    indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
    indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = device->CreateBuffer(&indexBufferDesc, &indexData, &_indexBuffer);
	if(FAILED(result))
		return false;

	// Release the index array since it is no longer needed.
	delete [] indices;
	indices = 0;

	return true;
}

void ParticleSystem::ShutdownBuffers()
{
	// Release the index buffer.
	if(_indexBuffer)
	{
		_indexBuffer->Release();
		_indexBuffer = 0;
	}

	// Release the vertex buffer.
	if(_vertexBuffer)
	{
		_vertexBuffer->Release();
		_vertexBuffer = 0;
	}

	return;
}

void ParticleSystem::EmitParticles(float frameTime, int frameRate)
{
	bool emitParticle, found;
	D3DXVECTOR3 position, velocity;
	D3DXVECTOR4 color;
	float lifeTime;
	int index, i, j;
	int maxParticles;

	// Increment the frame time.
	_accumulatedTime += frameTime;

	// Set emit particle to false for now.
	emitParticle = false;
	
	// Check if it is time to emit a new particle or not.
	if(_accumulatedTime > (1000.0f / _particlesPerSecond))
	{
		_accumulatedTime = 0.0f;
		emitParticle = true;
	}

	// Throttle the particle system if we're running at low FPS
	if(frameRate <= _minFps)
		maxParticles = _maxParticlesAtLowFps;
	else
		 maxParticles = _maxParticles;

	// If there are particles to emit then emit one per frame.
	if(emitParticle == true && _currentParticleCount < maxParticles - 1)
	{
		_currentParticleCount++;

		// Now generate the randomized particle properties.
		position = _systemPosition;
		position.x += (((float)rand()-(float)rand())/RAND_MAX) * _particleDeviation.x;
		position.y += (((float)rand()-(float)rand())/RAND_MAX) * _particleDeviation.y;
		position.z += (((float)rand()-(float)rand())/RAND_MAX) * _particleDeviation.z;

		lifeTime = 0;

		velocity = D3DXVECTOR3(_particleVelocity + (((float)rand()-(float)rand())/RAND_MAX) * _particleVelocityVariation.x,
			_particleVelocity + (((float)rand()-(float)rand())/RAND_MAX) * _particleVelocityVariation.y,
			-abs(_particleVelocity + (((float)rand()-(float)rand())/RAND_MAX) * _particleVelocityVariation.z));
		if(rand() % 100 > 50) velocity.x *= -1;
		if(rand() % 100 > 50) velocity.y *= -1;

		color.x   = (((float)rand()-(float)rand())/RAND_MAX) + 0.5f;
		color.y = (((float)rand()-(float)rand())/RAND_MAX) + 0.5f;
		color.z  = (((float)rand()-(float)rand())/RAND_MAX) + 0.5f;
		color.w  = 1;

		// Now since the particles need to be rendered from back to front for blending we have to sort the particle array.
		// We will sort using Z depth so we need to find where in the list the particle should be inserted.
		index = 0;
		found = false;
		while(!found)
		{
			if((_particleList[index].active == false) || (_particleList[index].position.z < position.z))
				found = true;
			else
				index++;
		}

		// Now that we know the location to insert into we need to copy the array over by one position from the index to make room for the new particle.
		i = _currentParticleCount;
		j = i - 1;

		while(i != index)
		{
			_particleList[i].position.x = _particleList[j].position.x;
			_particleList[i].position.y = _particleList[j].position.y;
			_particleList[i].position.z = _particleList[j].position.z;
			_particleList[i].lifeTime   = _particleList[j].lifeTime;
			_particleList[i].color.x    = _particleList[j].color.x;
			_particleList[i].color.y    = _particleList[j].color.y;
			_particleList[i].color.z    = _particleList[j].color.z;
			_particleList[i].color.w    = _particleList[j].color.w;
			_particleList[i].velocity   = _particleList[j].velocity;
			_particleList[i].active     = _particleList[j].active;
			i--;
			j--;
		}

		// Now insert it into the particle array in the correct depth order.
		_particleList[index].position.x = position.x;
		_particleList[index].position.y = position.y;
		_particleList[index].position.z = position.z;
		_particleList[index].lifeTime   = lifeTime;
		_particleList[index].color.x    = color.x;
		_particleList[index].color.y    = color.y;
		_particleList[index].color.z    = color.z;
		_particleList[index].color.w    = color.w;
		_particleList[index].velocity   = velocity;
		_particleList[index].active     = true;
	}

	return;
}

void ParticleSystem::UpdateParticles(float frameTime)
{
	int i;

	// Each frame we update all the particles by making them move using their position, velocity, and the frame time.
	for(i=0; i<_currentParticleCount; i++)
	{
		_particleList[i].position = _particleList[i].position + (_particleList[i].velocity * frameTime * 0.001f);
		_particleList[i].lifeTime += frameTime;

		// If the particle is in its last second of its lifetime, modify the alpha so it fades out
		if(_maxLifeTime - _particleList[i].lifeTime < 1000)
			_particleList[i].color.w = (_maxLifeTime - _particleList[i].lifeTime) / 1000;
	}

	return;
}


void ParticleSystem::KillParticles()
{
	int i, j;

	// Kill all the particles older than a specified time
	for(i=0; i<_maxParticles; i++)
	{
		if((_particleList[i].active == true) && _particleList[i].lifeTime > _maxLifeTime)
		{
			_particleList[i].active = false;
			_currentParticleCount--;

			// Now shift all the live particles back up the array to erase the destroyed particle and keep the array sorted correctly.
			for(j=i; j<_maxParticles-1; j++)
			{
				_particleList[j].position.x = _particleList[j+1].position.x;
				_particleList[j].position.y = _particleList[j+1].position.y;
				_particleList[j].position.z = _particleList[j+1].position.z;
				_particleList[j].lifeTime   = _particleList[j+1].lifeTime;
				_particleList[j].color.x    = _particleList[j+1].color.x;
				_particleList[j].color.y    = _particleList[j+1].color.y;
				_particleList[j].color.z    = _particleList[j+1].color.z;
				_particleList[j].color.w    = _particleList[j+1].color.w;
				_particleList[j].velocity   = _particleList[j+1].velocity;
				_particleList[j].active     = _particleList[j+1].active;
			}
		}
	}

	return;
}


bool ParticleSystem::UpdateBuffers(ID3D11DeviceContext* deviceContext)
{
	int index, i;
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;

	// Initialize vertex array to zeros at first.
	memset(_vertices, 0, (sizeof(VertexType) * _vertexCount));

	// Now build the vertex array from the particle list array.  Each particle is a quad made out of two triangles.
	index = 0;

	for(i=0; i<_currentParticleCount; i++)
	{
		// Bottom left.
		_vertices[index].position = D3DXVECTOR3(_particleList[i].position.x - _particleSize, _particleList[i].position.y - _particleSize, _particleList[i].position.z);
		_vertices[index].texture = D3DXVECTOR2(0.0f, 1.0f);
		_vertices[index].color = D3DXVECTOR4(_particleList[i].color.x, _particleList[i].color.y, _particleList[i].color.z, _particleList[i].color.w);
		index++;

		// Top left.
		_vertices[index].position = D3DXVECTOR3(_particleList[i].position.x - _particleSize, _particleList[i].position.y + _particleSize, _particleList[i].position.z);
		_vertices[index].texture = D3DXVECTOR2(0.0f, 0.0f);
		_vertices[index].color = D3DXVECTOR4(_particleList[i].color.x, _particleList[i].color.y, _particleList[i].color.z, _particleList[i].color.w);
		index++;

		// Bottom right.
		_vertices[index].position = D3DXVECTOR3(_particleList[i].position.x + _particleSize, _particleList[i].position.y - _particleSize, _particleList[i].position.z);
		_vertices[index].texture = D3DXVECTOR2(1.0f, 1.0f);
		_vertices[index].color = D3DXVECTOR4(_particleList[i].color.x, _particleList[i].color.y, _particleList[i].color.z, _particleList[i].color.w);
		index++;

		// Bottom right.
		_vertices[index].position = D3DXVECTOR3(_particleList[i].position.x + _particleSize, _particleList[i].position.y - _particleSize, _particleList[i].position.z);
		_vertices[index].texture = D3DXVECTOR2(1.0f, 1.0f);
		_vertices[index].color = D3DXVECTOR4(_particleList[i].color.x, _particleList[i].color.y, _particleList[i].color.z, _particleList[i].color.w);
		index++;

		// Top left.
		_vertices[index].position = D3DXVECTOR3(_particleList[i].position.x - _particleSize, _particleList[i].position.y + _particleSize, _particleList[i].position.z);
		_vertices[index].texture = D3DXVECTOR2(0.0f, 0.0f);
		_vertices[index].color = D3DXVECTOR4(_particleList[i].color.x, _particleList[i].color.y, _particleList[i].color.z, _particleList[i].color.w);
		index++;

		// Top right.
		_vertices[index].position = D3DXVECTOR3(_particleList[i].position.x + _particleSize, _particleList[i].position.y + _particleSize, _particleList[i].position.z);
		_vertices[index].texture = D3DXVECTOR2(1.0f, 0.0f);
		_vertices[index].color = D3DXVECTOR4(_particleList[i].color.x, _particleList[i].color.y, _particleList[i].color.z, _particleList[i].color.w);
		index++;
	}
	
	// Lock the vertex buffer.
	result = deviceContext->Map(_vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
		return false;

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)_vertices, (sizeof(VertexType) * _vertexCount));

	// Unlock the vertex buffer.
	deviceContext->Unmap(_vertexBuffer, 0);

	return true;
}

void ParticleSystem::RenderBuffers(ID3D11DeviceContext* deviceContext)
{
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
    stride = sizeof(VertexType); 
	offset = 0;
    
	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &_vertexBuffer, &stride, &offset);

    // Set the index buffer to active in the input assembler so it can be rendered.
    deviceContext->IASetIndexBuffer(_indexBuffer, DXGI_FORMAT_R32_UINT, 0);

    // Set the type of primitive that should be rendered from this vertex buffer.
    deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	return;
}