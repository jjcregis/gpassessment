#ifndef _CPU_H_
#define _CPU_H_

// Linking
#pragma comment(lib, "pdh.lib")

// Includes
#include <pdh.h>

class Cpu
{
public:
	Cpu();
	Cpu(const Cpu&);
	~Cpu();

	void Initialize();
	void Shutdown();
	void Frame();
	int GetCpuPercentage();

private:
	bool _canReadCpu;
	HQUERY _queryHandle;
	HCOUNTER _counterHandle;
	unsigned long _lastSampleTime;
	long _cpuUsage;
};

#endif