#ifndef _SHADERMANAGER_H_
#define _SHADERMANAGER_H_

// Includes
#include "D3DCore.h"
#include "LightShader.h"
#include "TextureShader.h"
#include "MultiTextureShader.h"
#include "BumpMapShader.h"
#include "ParticleShader.h"

class ShaderManager
{
public:
	ShaderManager();
	ShaderManager(const ShaderManager&);
	~ShaderManager();

	bool Initialize(ID3D11Device*, HWND, bool);
	void Shutdown();

	bool RenderLightShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*, 
						   D3DXVECTOR3, D3DXVECTOR4, D3DXVECTOR4);
	bool RenderTextureShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*);
	bool RenderMultiTextureShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView**);
	bool RenderBumpMapShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView**, 
							 D3DXVECTOR3, D3DXVECTOR4, D3DXVECTOR4);
	bool RenderParticleShader(ID3D11DeviceContext*, int, D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, ID3D11ShaderResourceView*);

private:
	LightShader* _lightShader;
	TextureShader* _textureShader;
	MultiTextureShader* _multiTextureShader;
	BumpMapShader* _bumpMapShader;
	ParticleShader* _particleShader;
};

#endif