#include "Camera.h"

Camera::Camera()
{
	_position = D3DXVECTOR3(0, 0, 0);
	_rotation = D3DXVECTOR3(0, 0, 0);
}


Camera::Camera(const Camera& other)
{
}


Camera::~Camera()
{
}

bool Camera::Initialize(Input* input)
{
	_input = input;
	return true;
}

void Camera::SetPosition(float x, float y, float z)
{
	_position = D3DXVECTOR3(x, y, z);
	return;
}


void Camera::SetRotation(float x, float y, float z)
{
	_rotation = D3DXVECTOR3(x, y, z);
	return;
}

void Camera::Translate(float x, float y, float z)
{
	_position += D3DXVECTOR3(x, y, z);
	return;
}

void Camera::Rotate(float x, float y, float z)
{
	_rotation += D3DXVECTOR3(x, y, z);
	return;
}

D3DXVECTOR3 Camera::GetPosition()
{
	return _position;
}


D3DXVECTOR3 Camera::GetRotation()
{
	return _rotation;
}


void Camera::Render()
{
	D3DXVECTOR3 up, lookAt;
	D3DXMATRIX rotationMatrix;
	
	up = D3DXVECTOR3(0, 1, 0);
	lookAt = D3DXVECTOR3(0, 0, 1);

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, _rotation.y, _rotation.x, _rotation.z);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&lookAt, &lookAt, &rotationMatrix);
	D3DXVec3TransformCoord(&up, &up, &rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAt = _position + lookAt;
	
	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&_viewMatrix, &_position, &lookAt, &up);

	return;
}


void Camera::GetViewMatrix(D3DXMATRIX& viewMatrix)
{
	viewMatrix = _viewMatrix;
	return;
}

void Camera::Shutdown()
{
}

bool Camera::Frame(float frameTime)
{
	_input->Frame(this, frameTime);
	return true;
}